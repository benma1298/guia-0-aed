
#include <iostream>
#include <string>
#include <list>
#include "Proteina.h"
using namespace std;

Proteina::Proteina(string nombre, string id){
	this->nombre = nombre;
	this->id = id;
}


string Proteina::getNombre(){
	return this->nombre;
}
string Proteina::getId(){
	return this->id;
}
list<Cadena> Proteina::getCadena(){
	return this->cadenas;
}
		

void Proteina::setNombre(string nombre){
	this->nombre = nombre;
}
void Proteina::setId(string id){
	this->id = id;
}
void Proteina::addCadena(Cadena cadena){
	this->cadenas.push_back(cadena);
}
