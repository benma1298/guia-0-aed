
#include <iostream>
#include <string>
#include <list>
#include "Aminoacido.h"
using namespace std;

Aminoacido::Aminoacido(string nombre, int numero){
	this->nombre = nombre;
	this->numero = numero;	
}


string Aminoacido::getNombre(){
	return this->nombre;
}
int Aminoacido::getNumero(){
	return this->numero;
}
list<Atomo> Aminoacido::getAtomo(){
	return this->atomos;
}


void Aminoacido::setNombre( string nombre){
	this->nombre = nombre;
}
void Aminoacido::setNumero(int numero){
	this->numero = numero;
}
void Aminoacido::addAtomo(Atomo atomo){
	this->atomos.push_back(atomo);
}
