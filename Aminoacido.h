
#include <iostream>
#include <string>
#include <list>
#include "Atomo.h"

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido {
    private:
        string nombre;
        int numero;
        list<Atomo> atomos;
        
    public:
        // Constructores
        Aminoacido();
        Aminoacido(string nombre, int numero);

        // Getters y Setters
		string getNombre();
        int getNumero();
        list<Atomo> getAtomo();
       
        void setNombre(string nombre);
        void setNumero(int numero);
        void addAtomo(Atomo atomo);
};
#endif 
