#include <iostream>
#include <string>
#include "Atomo.h"
#include "Coordenada.h"
using namespace std;


Atomo::Atomo (string nombre, int numero, Coordenada coordenada){
	this->nombre = nombre;
	this->numero = numero;
	this->coordenada = coordenada;
}
		

string Atomo::getNombre(){
	return this->nombre;
}
int Atomo::getNumero(){
	return this->numero;
}
Coordenada Atomo::getCoordenada(){
	return this->coordenada;
}


void Atomo::setNombre( string nombre){
	this->nombre = nombre;
}
void Atomo::setNumero(int numero){
	this->numero = numero;
}
void Atomo::setCoordenada(Coordenada coordenada){
	this->coordenada = coordenada;
}