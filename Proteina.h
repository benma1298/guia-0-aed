
#include <iostream>
#include <string>
#include <list>
#include "Cadena.h"

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina{
	private: 
		string nombre;
		string id;
		list<Cadena> cadenas;
		
	public:
		// Constructores
		Proteina(string nombre, string id);
		
        // Getters y Setters
		string getNombre();
		string getId();
		list<Cadena> getCadena();
		
		void setNombre(string nombre);
		void setId(string id);
		void addCadena(Cadena cadena);
		
};
#endif
	