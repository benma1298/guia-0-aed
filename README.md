# guia 0 aed

Guia numero 1 Algoritmos y estructura de Datos.
Autor: Benjamin Martin Albornoz.
Fecha Entrega: 29 de Agosto.
Base de datos: Proteinas.

Dado el diagrama de clases presente en la guia numero 0 Unidad 1 de Algoritmos y estructura de Datos,
Escribir un Programa en C++ que implemente dichas clases.
Este Programa debe permitir el ingreso de una lista de Proteinas con su informacion asociada.

Se crearon las clases:
Programa.cpp, Proteina.cpp, Proteina.h, Cadena.cpp, Cadena.h, Aminoacido.cpp, Aminoacido.h
Atomo.cpp, Atomo.h, Coordenada.cpp, Coordenada.h

Aparte de esto, se instauro un pequeno menu de 3 opciones en la Clase Programa.cpp, en donde se uso la tecnica flag
ademas de una serie de if, else if y else.
La opcion 1 de este menu nos deriva al metodo "ingresarProteina" en donde el programa nos pedira ingresar la informacion 
respectiva a la o las proteinas que queramos anadir a la base de datos.
La opcion 2 de este menu nos deriva al metodo "imprimirDatosProteina" en donde el programa nos entregara la informacion detallada
de todas las proteinas que se encuentren en esta base de datos
Por ultimo, la opcion 3, nos deriva al mensaje "El programa se dara por finalizado".
