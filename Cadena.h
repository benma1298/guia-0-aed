
#include <iostream>
#include <string>
#include <list>
#include "Aminoacido.h"

#ifndef CADENA_H
#define CADENA_H

class Cadena{
	
	private:
		string letra;
		list<Aminoacido> aminoacidos;
		
	public:
        // Constructores
        Cadena();
		Cadena(string letra);
		
        // Getters y Setters
		string getLetra();
		list<Aminoacido> getAminoacido();
		
		void setLetra(string letra);
		void addAminoacido(Aminoacido aminoacido);
};
#endif	
