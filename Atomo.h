
#include <iostream>
#include <string>
#include "Coordenada.h"

#ifndef ATOMO_H
#define ATOMO_H

using namespace std;

class Atomo{
	private:
		string nombre;
		int numero;
		Coordenada coordenada;
		
	public:
        // Constructores
        Atomo();
		Atomo(string nombre, int numero, Coordenada coordenada);
		
        // Getters y Setters
		string getNombre();
		int getNumero();
		Coordenada getCoordenada();
		
		void setNombre(string nombre);
		void setNumero(int numero);
		void setCoordenada(Coordenada coordenada);
};
#endif 
