
#include <iostream>
#include <string>
#include <list>
#include "Atomo.h"
#include "Coordenada.h"
#include "Aminoacido.h"
#include "Cadena.h"
#include "Proteina.h"
using namespace std;

Proteina ingresarProteina(){
	string nombre;
	string id;
	int cantidadCadenas;
	cin.ignore();
    cout << "Usted a elegido la opcion 1." << endl;
	cout << "Ingresando nueva Proteina" << endl;
	cout << "Ingrese el Nombre de la Proteina:";
	getline(cin, nombre);
	cout << "Ingrese la ID de la Proteina:";
	cin.ignore();
	getline(cin,id);
	Proteina proteina = Proteina(nombre, id);
	
	cout << "Ingrese cantidad de Cadenas" << endl;
	cin >> cantidadCadenas;
	for(int i = 0; i < cantidadCadenas; i++){
		string letra;
		int cantidadAminoacido;
		cout << "Ingrese la Letra de la Cadena:";
		cin.ignore();
		getline(cin, letra);
		Cadena cadena = Cadena(letra);
		
		cout << "Ingrese cantidad de Aminoacido:" << endl;
		cin >> cantidadAminoacido;
		for(int i = 0; i < cantidadAminoacido; i++){
			string nuevoAmino;
			int numeroAminoacido;
			int cantidadAtomos;
			cout << "Ingrese el Nombre del Aminoacido:";
			cin.ignore();
			getline(cin,nuevoAmino);
			cout << "Ingrese el Numero del Aminoacido:";
			cin >> numeroAminoacido;
			Aminoacido aminoacido = Aminoacido(nuevoAmino, numeroAminoacido);
			
			cout << "Ingrese cantidad de Atomos del Aminoacido:" << endl;
			cin >> cantidadAtomos;
			for(int i = 0; i < cantidadAtomos; i++){
				string nuevoAtomo;
				int numeroAtomo;
				int x;
				int y;
				int z;
				cout << "Ingrese el Nombre del Atomo:";
				cin.ignore();
				getline(cin, nuevoAtomo);
				cout << "Ingrese el Numero del Atomo:";
				cin >> numeroAtomo;
				cout << "Ingrese las Coordenadas del Atomo:" << endl;
				cout << "Coordenada X:";
				cin >> x;
				cout << "Coordenada Y:";
				cin >> y;
				cout << "Coordenada Z:";
				cin >> z;
				Coordenada coordenada = Coordenada(x, y, z);

				Atomo atomo = Atomo(nuevoAtomo, numeroAtomo, coordenada);

				aminoacido.addAtomo(atomo);	
			}
			cadena.addAminoacido(aminoacido);	
		}
		proteina.addCadena(cadena);
	}
	return proteina;
}

void imprimirDatosProteina(list<Proteina> proteinas){
	
	for(Proteina proteina: proteinas){
		cout << "Usted a elegido la opcion 2." << endl;
		cout << "Se mostraran los datos de la Base de datos: Proteinas" << endl;
		cout << endl;
		cout << "Nombre de la Proteina: " << proteina.getNombre() << endl;
		cout << "Id de la Proteina: " << proteina.getId() << endl;
		cout << "Cantidad de cadenas de la Proteina: " << proteina.getCadena().size() << endl;
		cout << endl;

		for(Cadena cadena: proteina.getCadena()){
			cout << endl;
			cout << "Letra de la cadena: " << cadena.getLetra() << endl;
			cout << "Cantidad de aminoacidos de la cadena: " << cadena.getAminoacido().size() << endl;
			cout << endl;

			for(Aminoacido aminoacido: cadena.getAminoacido()){
				cout << endl;
				cout << "Nombre del Aminoacido: " << aminoacido.getNombre() << endl;
				cout << "Numero del Aminoacido: " << aminoacido.getNumero() << endl;
				cout << "Cantidad de Atomos del Aminoacido: " << aminoacido.getAtomo().size() << endl;
				cout << endl;

				for(Atomo atomo: aminoacido.getAtomo()){
					cout << endl;
					cout << "Nombre del Atomo: " << atomo.getNombre() << endl;
					cout << "Numero del Atomo: " << atomo.getNumero() << endl;
					cout << "Coordenada X del Atomo: " << atomo.getCoordenada().getX() << endl;
					cout << "Coordenada Y del Atomo: " << atomo.getCoordenada().getY() << endl;
					cout << "Coordenada Z del Atomo: " << atomo.getCoordenada().getZ() << endl;
					cout << endl;
				}
			}
		}
	}
}

int main(){
	list<Proteina> proteinas;
	imprimirDatosProteina(proteinas);
	int flag = 0;
	int menu;
	while(flag == 0){
		cout << "Base de datos: Proteinas" << endl;
		cout << "Bienvenido al Menu" << endl;
		cout << "Eliga el numero de la opcion que desea:" << endl;
		cout << "1.- Ingresar una nueva Proteína" << endl;
		cout << "2.- Mostrar los datos de las proteínas" << endl;
		cout << "3.- Finalizar Base de Datos" << endl;
		cin >> menu;
		if(menu == 1){
			 proteinas.push_back(ingresarProteina());
		}
		else if(menu == 2){
			if(proteinas.size() == 0){
				cout << "Aun no existen Proteinas" << endl;
			}
			else{
				imprimirDatosProteina(proteinas);
			}
		}
		else if(menu == 3){
			cout << "Usted a elegido la opcion 3." << endl;
			cout << "El programa se dara por finalizado" << endl;
			flag = 1;
		}
		else{
			cout << "Porfavor, ingrese una opcion valida" << endl;
		}	
	}
	return 0;
}