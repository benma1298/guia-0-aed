
#include <iostream>
#ifndef COORDENADA_H
#define COORDENADA_H

class Coordenada {
    private:
        float x;
        float y;
        float z;
        
    public:
        // Constructores
        Coordenada ();
        Coordenada (float x, float y, float z);
        
        // Getters y Setters
		float getX();
        float getY();
        float getZ();
       
        void setX(float x);
        void setY(float y);
        void setZ(float z);
};
#endif
